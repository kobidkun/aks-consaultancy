<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






Route::get('/', function () {
    return view('pages.homepage');
})->name('homepage');

Route::get('/incometax', function () {
    return view('pages.incometax');
})->name('pages.incometax');


Route::get('/iec-code', function () {
    return view('pages.export');
})->name('pages.export');









Route::get('/services/tally', function () {
    return view('pages.tally');
})->name('tally');

Route::get('/services/mutual-fund', function () {
    return view('pages.mutualfund');
})->name('mf');

Route::get('/services/dsc', function () {
    return view('pages.dsc');
})->name('dsc');

Route::get('/services/bus', function () {
    return view('pages.bus');
})->name('bus');

Route::get('/services/lic', function () {
    return view('pages.lic');
})->name('lic');

Route::get('/services/gst', function () {
    return view('pages.gst');
})->name('gst');

Route::get('/services/project-finance', function () {
    return view('pages.proj');
})->name('proj');

Route::get('/services/accounting', function () {
    return view('pages.accounting');
})->name('accounting');

Route::get('/services/contact', function () {
    return view('pages.contact');
})->name('contact');

Route::get('/services/hsn', function () {
    return view('pages.hsn');
})->name('hsn');
    Route::get('/services/company', function () {
    return view('pages.company');
})->name('company');


    Route::get('/services/import-bank-statement-tally', function () {
    return view('pages.bankstatement');
})->name('import.bank.statement');

    Route::get('/services/sheetmapping', function () {
    return view('pages.sheetmapping');
})->name('mapping');

Route::get('home/search', 'HSNController@mySearch')->name('hsn.search');




Route::get('/videos', function () {

    $a = \App\Video::all();


    return view('pages.video')->with(['v' => $a]);
})->name('public.video');








Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('home/hsnsave', 'HSNController@store')->name('hsn.store');
Route::post('home/sacsave', 'HSNController@storesac')->name('sac.store');

Route::get('/home/hsn/create', function () {
    return view('pages.internal.hsn.create');
})->name('create.hsn');


Route::get('/home/sac/create', function () {
    return view('pages.internal.sac.create');
})->name('create.sac');

Route::get('/hsn', 'HSNController@index')->name('hsn.all');
Route::get('/sac', 'HSNController@sacindex')->name('sac.all');




Route::get('/hsn/edit/{id}', 'HSNController@edithsn')->name('hsn.all.edit');
Route::get('/sac/edit/{id}', 'HSNController@editsac')->name('sac.all.edit');


//blog
Route::any('/blog', 'BlogController@index')->name('blog.all');
Route::any('/blog/{id}', 'BlogController@show')->name('blog.show');






Route::any('/hsn/update/{id}', 'HSNController@updatehsn')->name('hsn.all.update');
Route::any('/sac/update/{id}', 'HSNController@updatesac')->name('sac.all.update');



Route::resource('admin/video', 'VideoController');



Route::any('/admin/video/edit/extra/{id}', 'VideoController@edit')->name('extra.video.show');
Route::any('/admin/video/update/extra/{id}', 'VideoController@update')->name('extra.video.update');
Route::any('/admin/video/delete/extra/{id}', 'VideoController@destroy')->name('extra.video.delete');



Route::any('/admin/blog/all', 'Adminblog@all')->name('admin.blog.all');

Route::any('/blog', 'BlogController@index')->name('blog.all');
Route::any('/admin/blog/create', 'Adminblog@create')->name('admin.blog.create');
Route::any('/admin/blog/store', 'Adminblog@store')->name('admin.blog.store');
Route::any('/admin/blog/edit/extra/{id}', 'Adminblog@edit')->name('admin.blog.edit');
Route::any('/admin/blog/update/extra/{id}', 'Adminblog@update')->name('admin.blog.update');
Route::any('/admin/blog/delete/extra/{id}', 'Adminblog@destroy')->name('admin.blog..delete');