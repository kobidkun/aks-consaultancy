<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class Sac extends Model
{
    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'sacs.code' => 1,
            'sacs.description' => 4,
        ]
    ];
}
