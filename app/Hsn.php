<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class Hsn extends Model
{
    use SearchableTrait;


    //

    protected $searchable = [
        'columns' => [
            'hsns.code' => 1,
            'hsns.description' => 4,
        ]
    ];
}
