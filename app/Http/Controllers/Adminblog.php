<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use Illuminate\Http\Request;

class Adminblog extends Controller
{
    public  function all(){
        $a = Blog::paginate(12);
        return view('pages.internal.blog.all')->with(['results' => $a]);
    }

    public  function create(){

        return view('pages.internal.blog.create');
    }

    public  function edit($id){

        $a = Blog::findorfail($id);

        return view('pages.internal.blog.edit')->with(['a' => $a]);
    }

    public  function store(Request $request){

        $a = new Blog();
        $a->heading = $request->title;
        $a->description = $request->description;
        $a->save();

        return redirect(route('admin.blog.all')) ;

    }


    public  function update(Request $request, $id){

        $a = Blog::findorfail($id);
        $a->heading = $request->title;
        $a->description = $request->description;
        $a->save();

        return redirect(route('admin.blog.all')) ;

    }
}
