<?php

namespace App\Http\Controllers;

use App\Hsn;
use App\Sac;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use DB;
class HSNController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $results =  DB::table('hsns')->simplePaginate(15);

//dd($results);
        return view('pages.internal.hsn.all')->with(['results' => $results]);

    }

    public function sacindex()
    {
        $results =  DB::table('sacs')->simplePaginate(15);

//dd($results);
        return view('pages.internal.sac.all')->with(['results' => $results]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $s = new Hsn();
        $s->chapter = $request->chapter;
        $s->code = $request->code;
        $s->rate = $request->rate;
        $s->xrate = $request->xrate;
        $s->cess = $request->cess;
        $s->effective_from = $request->effective_from;
        $s->description = $request->description;
        $s->related = $request->related;
        $s->save();
        return back();
    }

    public function storesac(Request $request)
    {
        $s = new Sac();
        $s->chapter = $request->chapter;
        $s->code = $request->code;
        $s->rate = $request->rate;
        $s->xrate = $request->xrate;
        $s->cess = $request->cess;
        $s->effective_from = $request->effective_from;
        $s->description = $request->description;
        $s->related = $request->related;
        $s->save();
        return back();
    }


    public function mySearch(Request $request)
    {

        $search = $request->search;
       // if($request->has('search')){
            $results = Hsn::search($request->get('search'))->get();
            $sacs = Sac::search($request->get('search'))->get();
        //   }



        return view('pages.resulthsn')->with(['results'=>$results,'sacs' => $sacs,'search' => $search]);
       // return view('my-search', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edithsn($id)
    {

        $results = Hsn::findorfail($id);

        return view('pages.internal.hsn.edit')->with(['results' => $results]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editsac($id)
    {

        $results = Sac::findorfail($id);

        return view('pages.internal.hsn.edit')->with(['results' => $results]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatehsn(Request $request, $id)
    {
        $s = Hsn::findorfail($id);
        $s->chapter = $request->chapter;
        $s->code = $request->code;
        $s->rate = $request->rate;
        $s->xrate = $request->xrate;
        $s->cess = $request->cess;
        $s->effective_from = $request->effective_from;
        $s->description = $request->description;
        $s->related = $request->related;
        $s->save();
        return back();
    }

    public function updatesac(Request $request, $id)
    {
        $s = Sac::findorfail($id);
        $s->chapter = $request->chapter;
        $s->code = $request->code;
        $s->rate = $request->rate;
        $s->xrate = $request->xrate;
        $s->cess = $request->cess;
        $s->effective_from = $request->effective_from;
        $s->description = $request->description;
        $s->related = $request->related;
        $s->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
