@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                        <li>Sheet Mapping
                        </li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Sheet Mapping</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                        You may map your excel sheet with our standard templates, so there is no need to copy paste

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Map your excel sheet with our Standard templates
                        </h5>
                        <p>
                            <strong>You may map your excel sheet with our Standard templates, so there is no need to even copy paste. This MAPPPING process is one time for each sheet. Afterwards click only "Import to Entries Sheet" button and generate entries</strong>
                        </p>
                        <p>
                            Now you can import your excel data using Excel Formulas like a magic. You don't have to alter your existing excel sheet, just MAP your excel sheet columns with Excel to Tally Templates & thats all. While Mapping your sheet with Excel to Tally templates you can use Excel Formulas in column names to import calculated data.

                        </p>



                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
