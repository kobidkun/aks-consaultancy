@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                        <li>Bus Tracking</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Bus Tracking</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                      <h3>Leading the Way in Bus TrackingDiscover the real value of traceability to increase profitability</h3>

                        <ul class="landingbustrackingsystemulone">
                            <li>Lower maintenance costs</li>
                            <li>Safer driving means fewer accidents and claims</li>
                            <li>Maximize on time efficiency</li>
                        </ul>
                        <div class="landingbustrackingsystemulonenormaltext">Intelligence you can trust and savings you can bank - And make sure you fit the best</div>


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Bus Tracking</h5>
                        <ul class="landingbustrackingsystemheadsecthreul">
                            <li>Live Tracking helps to identify the current location of the bus</li>
                            <li>Track your bus using IOS/Android mobile application</li>
                            <li>Customize your bus tracking options and reduce the operating expenses while increasing the number of service calls completed each day</li>
                            <li>Reduce fuel cost by monitoring fuel consumption</li>
                            <li>Get control of and reduce overtime pay</li>
                            <li>Improves the efficiency, profitability and safety of most business vehicle fleets</li>
                            <li>Analyze the reports like over speed, harsh breaking, vehicle ideal time, halt time, ignition on/off to closely monitor the security compliance and Driver&rsquo;s behavior.</li>
                        </ul>


                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
