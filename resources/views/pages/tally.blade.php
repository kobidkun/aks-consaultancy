@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Services</a></li>
                        <li>Tally Services</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->


<style>
    .colorwhite :hover{
        background-color: #2D0063;
    }
</style>


<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Don't Worry About Errors, Notices & Penalties
                    </h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                       <ul>
                           <li> <i class="fa fa-check-circle"></i> Automatically identify data mismatches
                           </li>
                           <li> <i class="fa fa-check-circle"></i> No need to handle JSON files
                           </li>
                           <li><i class="fa fa-check-circle"></i> Save time by filing returns for all your clients from one dashboard
                           </li>
                           <li><i class="fa fa-check-circle"></i> Import data from Excel to avoid copy-pasting of data
                           </li>

                       </ul>




                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<section class="flat-row v8 parallax parallax5">
    <div class="section-overlay style3"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section style3 text-center color-white">
                    <h1 class="title">Features</h1>
                </div>
            </div>
        </div>
        <ul id="data-effect" class="data-effect wrap-iconbox clearfix colorwhite">
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_building"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Voucher Entries
                        </h5>
                        <p>Sales, Purchase, Receipt, Payment, Contra, Journal, Debit & Credit Notes, PO & SO, Stock Journal etc</p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-menu"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">GST, TDS</h5>
                        <p>Entries can be booked having statutory compliances like GST, TDS, can be booked with customization.
                        </p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_piechart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Entries having stock items
                        </h5>
                        <p>Multiple stock items with multiple ledgers allocation. even godown / batch / tracking etc also available</p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>

        </ul>

        <ul id="data-effect" class="data-effect wrap-iconbox clearfix colorwhite">
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_archive_alt"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">New Masters Detection

                        </h5>
                        <p>Automatic Ledgers / Items detection and creation while posting entries that makes posting even easier
                        </p>
                        <p></p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_search-2"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Ledgers / Items creation

                        </h5>
                        <p>Additional templates are available for creation of Ledgers, Stock Items, Sub Groups and Cost Centers etc.
                        </p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_key_alt"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Unique Entries Code

                        </h5>
                        <p>Duplication of entries can be restricted using this feature. Alteration of existing entries is now possible</p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>

        </ul>
        <ul id="data-effect" class="data-effect wrap-iconbox clearfix colorwhite">
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_link_alt"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Entries with Cost Centers


                        </h5>
                        <p>
                            Cost Centers & Cost Categories can be defined for entries, Multiple Cost Centers / categories for single entry
                        </p>
                        <p></p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>

            <li>
                <div class="iconbox effect text-center batchdesc2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-calendar"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Batch Wise Reports


                        </h5>
                        <p>
                            Item-Batch wise report shows the profitability and consumption details for each stock item
                        </p>
                        <p></p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>
            <li>
                <div class="iconbox effect text-center">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-alert"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">ERP 9 SMS Module


                        </h5>
                        <p>
                            Tally SMS  helps you Stay-in-Touch with your customers using the powerful medium of SMS
                        </p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>

 {{--           <li>
                <div class="iconbox effect text-center batchdesc2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-calendar"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Batch Wise Reports


                        </h5>
                        <p>
                            Item-Batch wise report shows the profitability and consumption details for each stock item
                        </p>
                        <p></p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>

            <li>
                <div class="iconbox effect text-center bankstatementheading">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_book"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Import Bank Statements


                        </h5>
                        <p>
                            Import downloaded Bank Statements in excel format as is. Multple Bank support and new banks can be added

                        </p>
                    </div>
                    <div class="effecthover"></div>
                </div>
            </li>--}}

        </ul>

    </div>


    <br>
    <br>
    <br>



    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-section style3 text-center color-white">
                    <h1 class="title">Tally Addons</h1>
                </div>
            </div>
        </div>



        <ul id="data-effect" class="data-effect wrap-iconbox clearfix colorwhite">
            <li>
                <div class="iconbox effect text-center bankstatementheading">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_book"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Import Bank Statements


                        </h5>
                        <p>
                            Import downloaded Bank Statements in excel format as is. Multple Bank support and new banks can be added

                        </p>
                    </div>

                    <br>
                    <a href="{{route('import.bank.statement')}}" class="flat-button">Read More</a>


                    <div class="effecthover"></div>
                </div>
            </li>

            <li>
                <div class="iconbox effect text-center batchdesc">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="icon_documents_alt"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Sheet Mapping


                        </h5>
                        <p>
                            You may map your excel sheet with our standard templates, so there is no need to copy paste
                        </p>
                    </div>
                    <br>
                    <a href="{{route('mapping')}}" class="flat-button">Read More</a>
                    <div class="effecthover"></div>
                </div>
            </li>



        </ul>
    </div>
</section>



@endsection

@section('footer')

    <style>
        .batchdesc{
            cursor: pointer;
        }

        .bankstatementheading{
            cursor: pointer;
        }
    </style>
    <script>
        $(".batchdesc").click(function() {
            $('html, body').animate({
                scrollTop: $(".batchdesc2").offset().top
            }, 2000);
        });


        $(".bankstatementheading").click(function() {
            $('html, body').animate({
                scrollTop: $(".bankstatedesc").offset().top
            }, 2000);
        });
    </script>
    @endsection