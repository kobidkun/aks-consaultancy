@extends('base')

@section('content')

    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Blog</h1>
                    </div><!-- /.page-title-captions -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="/">Home</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li>{{$a->heading}}</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <!-- Blog posts -->
    <section class="flat-row blog-list">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sidebar">
                        <div class="widget widget-nav-menu">
                            <ul class="widget-menu">


                                @foreach($bs as $b)


                                    <li><a href="{{route('blog.show', $b->id)}}">{{$b->heading}}</a></li>


                                    @endforeach


                            </ul>
                        </div>






                    </div>
                </div>



                <div class="col-lg-9">
                    <div class="post-wrap post-list">
                        <article class="entry border-shadow clearfix effect-animation"  data-animation="fadeInUp" data-animation-delay="0.2s" data-animation-offset="75%">
                            <div class="entry-border clearfix">
                                <div class="featured-post">

                                </div><!-- /.feature-post -->
                                <div class="content-post" style="padding: 20px">


                                    {!! $a->description !!}




                                </div><!-- /.contetn-post -->
                            </div><!-- /.entry-border -->
                        </article>



                    </div>

                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>



@endsection
