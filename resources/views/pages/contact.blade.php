@extends('base')

@section('content')


    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Contact Us</h1>
                    </div><!-- /.page-title-captions -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="{{route('homepage')}}">Home</a></li>
                            <li><a href="#">Contact</a></li>
                            <li>Style 1</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <section class="flat-row page-contact">
        <div class="container">
            <div class="wrap-infobox">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-maps box-shadow3 margin-bottom-73">
                            <div style="width: 100%; height: 520px; ">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d891.0035252451264!2d88.42473592915994!3d26.71199649895091!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e4416bfb2544fb%3A0xee8c7b1db0f42408!2sS.K.+Enterprise!5e0!3m2!1sen!2sin!4v1522712229343" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="info-box text-center">
                            <h3>Reach us</h3>
                            <ul>


                                <li>Venus More Hill Cart Road
                                    Siliguri, IN
                                </li>
                                <li>Email: info@aksconsultancyservice.com</li>
                                <li>Phone: 258-556-189</li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="container">
            <div class="wrap-formcontact">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="margin-left_12">
                            <form id="contactform" class="contactform style4 clearfix" method="post" action="./contact/contact-process.php" novalidate="novalidate">
                                <span class="flat-input"><input name="name" id="name" type="text" placeholder="Name*" required="required"></span>
                                <span class="flat-input"><input name="email" id="email" type="email" placeholder="Email" required="required"></span>

                                <span class="flat-input"><textarea name="message" placeholder="Messages" required="required"></textarea></span>
                                <span class="flat-input"><button name="submit" type="submit" class="flat-button" id="submit" title="Submit now">send messages</button></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection
