@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                        <li>Import Bank Statements

                        </li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Import Bank Statements
                    </h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                        Import Bank Statements from Excel to Tally.ERP9

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h3 class="box-title"> Import Bank Statements from Excel to Tally.ERP9

                        </h3>
                        <p>
                            AKS Consultancy Sevices has introduced a new unique feature. Now you can directly import your bank statements as is downloaded from bank's website. You dont have to change anything in excel sheet. Just import bank statement & directly book entries in Tally.ERP9 . These all entries will be posted to Suspenses Account. Afterward you can alter these entries in Tally
                        </p>


                        <br><br>




                        <h5>
                            Features

                        </h5>




                        <ul>
                            <li>
                                <i class="fa fa-check-circle"></i> You can manage multiple Banks as per your requirement

                            </li>

                            <li>
                                <i class="fa fa-check-circle"></i> NO Programming / XML / TDL knowledge required

                            </li>

                            <li>
                                <i class="fa fa-check-circle"></i> You can alter / add Ledger column to book these entries to their relative a/cs


                            </li>

                            <li>
                                <i class="fa fa-check-circle"></i> And much more

                            </li>


                        </ul>



                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
