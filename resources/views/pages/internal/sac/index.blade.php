@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New HSN Code</div>

                <form style="padding: 10px 10px 10px 10px"
                      method="post" action="{{route('hsn.store')}}"
                >

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="exampleInputEmail1">Chapter</label>
                        <input type="text"
                               name="chapter"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Chapter">

                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Code</label>
                        <input type="text"
                               name="code"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Code">

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Rate</label>
                        <input type="text"
                               name="rate"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Rate">

                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Cess</label>
                        <input type="text"
                               name="cess"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Cess">

                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Effective From</label>
                        <input type="text"
                               name="effective_from"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Effective From">

                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text"
                               name="description"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Description">

                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Related</label>
                        <input type="text"
                               name="related"
                               class="form-control"
                               id="exampleInputEmail1"
                               aria-describedby="emailHelp"
                               placeholder="Related">

                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
