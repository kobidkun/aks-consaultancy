@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New Sac Code</div>

                <table class="table">
                    <thead class="thead-light" style="background-color: #F1F1F1">
                    <tr>

                        <th scope="col">Chapter</th>

                        <th scope="col">HSN Code</th>
                        <th scope="col">Rate </th>
                        <th scope="col">CESS %
                        </th>
                        <th scope="col">Effective from </th>
                        <th scope="col">Description </th>
                        <th scope="col">Related Export Inport HSN Codes </th>
                        <th scope="col">Action </th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <th scope="row">{{$result->chapter}}</th>
                            <td>{{$result->code}}</td>
                            <td>{{$result->rate}}</td>
                            <td>{{$result->cess}}</td>
                            <td>{{$result->effective_from}}</td>
                            <td>{{$result->description}}</td>
                            <td>{{$result->related}}</td>
                            <td> <a href="{{route('hsn.all.edit',$result->id)}}"  class="btn btn-primary btn-block">Edit</a></td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
                {{ $results->links() }}

            </div>
        </div>
    </div>
</div>
@endsection
