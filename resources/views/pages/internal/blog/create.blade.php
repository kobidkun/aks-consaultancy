@extends('layouts.app')

@section('content')


    <script src="{{asset('tinymce/tinymce.min.js')}}"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New Blog</div>

                <form style="padding: 10px 10px 10px 10px"
                      method="post" action="{{route('admin.blog.store')}}"
                >

                    {{ csrf_field() }}


                    <div class="form-row">
                        <div class="col">
                            <input type="text"
                                   name="title"
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   required
                                   aria-describedby="emailHelp"
                                   placeholder="Title">
                        </div>

                    </div>

                    <br>


                    <div class="form-row">
                        <div class="col">


                            <textarea name="description"

                                      rows="20" cols="80"
                                      class="form-control my-editor">
                                4653
                            </textarea>








                        </div>
                    </div>



                    <div class="form-row">
                        <div class="col">










                        </div>
                    </div>

                    <br>



                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>

    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>




@endsection
