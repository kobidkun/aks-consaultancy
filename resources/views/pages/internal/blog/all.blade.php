@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">


                <a class="btn btn-success" href="/admin/blog/create">Create Bog Post</a>


                <div class="card-header">New HSN Code</div>

                <table class="table">
                    <thead class="thead-light" style="background-color: #F1F1F1">
                    <tr>

                        <th scope="col">id</th>

                        <th scope="col">title</th>
                        <th scope="col">Edit</th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($results as $result)
                        <tr>
                            <th scope="row">{{$result->id}}</th>
                            <td>{{$result->heading}}</td>


                            <td> <a href="{{route('admin.blog.edit',$result->id)}}"  class="btn btn-primary btn-block">Edit</a></td>
                        </tr>

                    @endforeach

                    </tbody>
                </table>
                {{ $results->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
