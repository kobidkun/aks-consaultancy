@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New HSN Code</div>

                <form style="padding: 10px 10px 10px 10px"
                      method="post" action="{{route('hsn.store')}}"
                >

                    {{ csrf_field() }}


                    <div class="form-row">
                        <div class="col">
                            <input type="text"
                                   name="chapter"
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   required
                                   aria-describedby="emailHelp"
                                   placeholder="Chapter">
                        </div>
                        <div class="col">
                            <input type="text"
                                   name="code" required
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Code">

                        </div>
                        <div class="col">

                            <input type="text"
                                   name="rate" required
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Rate">
                        </div>
                        <div class="col">
                            <input type="text"
                                   name="cess"
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Cess">

                        </div>

                        <div class="col">
                            <input type="text"  required
                                   name="effective_from"
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Effective From">

                        </div>

                        <div class="col">
                            <input type="text"
                                   name="description"  required
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Description">

                        </div>
                    </div>

                    <br>


                    <div class="form-row">
                        <div class="col">
                    <input type="text"
                           name="related"
                           class="form-control"
                           id="exampleInputEmail1"
                           aria-describedby="emailHelp"
                           placeholder="Related">

                        </div>
                    </div>

                    <br>



                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
