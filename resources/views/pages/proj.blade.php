@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Services</a></li>
                        <li>Tally Services</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Project Finance Service</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">

                         We provide Project Finance Support for Small and Medium Business.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Project Finance Service</h5>
                        <p>
                            Our professional colleagues are committed to look for projects owned by small and medium-sized companies with the most lucrative parameters, and invest in them in a diversified manner to minimise risks.

                            Investors looking for long-term, secure portfolios are at the right place with us.

                            We are looking forward to share our investment vision with you.




                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
