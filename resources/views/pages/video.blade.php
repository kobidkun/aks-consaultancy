@extends('base')

@section('content')


    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                            <li>Videos</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <!-- Services item -->
    <section class="flat-row blog-grid">
        <div class="container">




            <div class="post-wrap post-grid wrap-column clearfix">



                @foreach($v as $vs)
                    <article class="entry border-shadow flat-column3 clearfix">
                        <div class="entry-border clearfix">
                            <div class="featured-post">
                                <a href="#">

                                    <iframe width="350" height="200"
                                            src="https://www.youtube.com/embed/{{$vs->url}}"
                                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>



                                </a>
                            </div><!-- /.feature-post -->
                            <div class="content-post">

                                <h2 class="title-post"><a href="#">{{$vs->heading}}</a></h2>
                                <div class="meta-data style2 clearfix">
                                    <ul class="meta-post clearfix">
                                        <li class="day-time">
                                            <span>{{$vs->created_at}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div><!-- /.contetn-post -->
                        </div><!-- /.entry-border -->
                    </article>


                @endforeach














            </div>







        </div>
    </section>



@endsection
