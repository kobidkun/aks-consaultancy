@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Services</a></li>
                        <li>GST Filling services</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Simplifying GST compliance for your business</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                        We manage your books of accounts, generate E-way bills, maintain different stock groups and
                        file your GST returns

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">GST Return Filings</h5>
                        <p>
                            GST Return Filings process seems to be complicated in calculating and make it file according to distinct
                            rules of state government. But it is extremely easy with us. We support and save your valuable time
                            on GST Return Filings and sales tax return as fast as possible. Get our team guidance on online e- filing
                            sales tax, sales tax online payment and make your task as simple with our dedicated professionals. Our experts
                            make you experience the quicker way the process of
                            sales tax return filings from convenient begin to peaceful completion

                        </p>



                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
