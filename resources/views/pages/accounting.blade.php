@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Services</a></li>
                        <li>Tally Services</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Accounting</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                        The service involves preparing and maintaining day to day bookkeeping and monthly or quarterly management accounts

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Accounting Outsourcing</h5>
                        <br>
                        Book keeping and General Accounting Service:
                        The service involves preparing and maintaining day to day bookkeeping and monthly or quarterly management accounts.These books are prepared as per the required accounting standards and can also be made as per specific client instructions.
                        <br>

                        <br> <h5  class="box-title"> Preparation of Financial Statements:</h5>
                        The service involves preparing a company's annual accounts and schedules ready for the statutory annual audit.
                        <br>
                        <br>    <h5  class="box-title">  Payroll:</h5>
                        Preparation of the monthly payroll based on the inputs received from organization. The same would include all statutory and other relevant deductions as required.

                        Ensure deductions of all relevant amounts and as per applicable statutory laws like Income Tax, provident Fund and Professional Tax etc.

                        Checking Form 16 for the Employees and file Qtly. returns for e-TDS with Income Tax authorities.

                        Deposit TDS & provide proof of deposit.

                        Ensure redressal of any issues.

                        Reconciliation of payments/statutory deductions etc. with books of accounts on quarterly basis.
                        <br>
                        <br>  <h5  class="box-title"> Cash Forecasting and Cash Budgeting:</h5>
                        The service involves analyzing the cash requirements of the business and making cash forecasts for planning the future.

                        <br>
                        <br>

                        <h5  class="box-title">  Advantages:</h5>
                        Time saving <br>

                        Analyzing the potential risks in advance <br>

                        Correct interpretation of the company's financial position <br>

                        Informed and better managerial decision making <br>

                        Collection, summarization, and accurate analysis of financial data <br>

                        Optimization of business resources and processes <br>

                        Continuity of Work, i.e employee turnover will not affect the work as immediate replacement with existing team <br>

                        Cost Saving. <br>


                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
