@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="/">Mutual Fund</a></li>
                        <li>Mutual Fund</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">Mutual Fund</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                       Our money goes into the best Mutual Funds that have been scientifically selected without any bias.


                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">Mutual Fund Investment</h5>
                        <p>

                            To many people, Mutual Funds can seem complicated or intimidating. We are going to try and simplify it for you at its very basic level. Essentially, the money pooled in by a large number of people (or investors) is what makes up a Mutual Fund. This fund is managed by a professional fund manager.
                            <br>

                            It is a trust that collects money from a number of investors who share a common investment objective. Then, it invests the money in equities, bonds, money market instruments and/or other securities. Each investor owns units, which represent a portion of the holdings of the fund. The income/gains generated from this collective investment is distributed proportionately amongst the investors after deducting certain expenses, by calculating a scheme’s “Net Asset Value or NAV. Simply put, a Mutual Fund is one of the most viable investment options for the common man as it offers an opportunity to invest in a diversified, professionally managed basket of securities at a relatively low cost.


                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
