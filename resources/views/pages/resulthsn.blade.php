@extends('base')

@section('content')




<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">



        <style>
            input[type=text] {
                width: 80%;
            }

            .icon{
                background-color: #FF581D;
                color: #ffffff;

            }


            .nav-item .active{

                background-color: #FF581D;

            }


        </style>


        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">


                    <div class="box-content">




                            <div class="col-lg-12">


                                <form method="get" action="{{route('hsn.search')}}">
                                    <input name="search" type="text" placeholder="Search by HSN Code (e.g. 11090000) or Keywords (e.g. horses cotton handloom machines)" required="required">
                                    <span><button type="submit" class="icon"><i class="fa fa-search"></i></button></span>

                                </form>


                            </div>



                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home">HSN &nbsp ({{$results->count()}}) &nbsp &nbsp &nbsp </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu1">SAC ({{$sacs->count()}} )</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="home" class="container tab-pane active"><br>
                                <table class="table">
                                    <thead class="thead-light" style="background-color: #F1F1F1">
                                    <tr>

                                        <th scope="col">Chapter</th>

                                        <th scope="col">HSN Code</th>
                                        <th scope="col">Rate </th>
                                        <th scope="col">CESS %
                                        </th>
                                        <th scope="col">Effective from </th>
                                        <th scope="col">Description </th>
                                        <th scope="col">Related Export Inport HSN Codes </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($results as $result)
                                        <tr>
                                            <th scope="row">{{$result->chapter}}</th>
                                            <td>{{$result->code}}</td>
                                            <td>{{$result->rate}}</td>
                                            <td>{{$result->cess}}</td>
                                            <td>{{$result->effective_from}}</td>
                                            <td>{{$result->description}}</td>
                                            <td>{{$result->related}}</td>
                                        </tr>

                                        @endforeach

                                    </tbody>
                                </table>



                            </div>
                            <div id="menu1" class="container tab-pane fade"><br>
                                <table class="table">
                                    <thead class="thead-light" style="background-color: #F1F1F1">
                                    <tr>
                                        <th scope="col">Description</th>
                                        <th scope="col">SAC Code</th>
                                        <th scope="col">Rate %
                                        </th>
                                        <th scope="col">Also Check </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sacs as $sac)
                                        <tr>
                                            <td>{{$sac->description}}</td>
                                            <td>{{$sac->code}}</td>
                                            <td>{{$sac->rate}}</td>


                                            <td>{{$sac->related}}</td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>


                            </div>

                        </div>





                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
