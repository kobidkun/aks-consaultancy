@extends('base')

@section('content')




<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">



        <style>
            input[type=text] {
                width: 80%;
            }

            .icon{
                background-color: #FF581D;
                color: #ffffff;

            }


            .nav-item .active{

                background-color: #FF581D;

            }


        </style>


        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">


                    <div class="box-content">




                            <div class="col-lg-12">


                                <form method="get" action="{{route('hsn.search')}}">
                                    <input name="search" type="text" placeholder="Search by HSN Code (e.g. 11090000) or Keywords (e.g. horses cotton handloom machines)" required="required">
                                    <span><button type="submit" class="icon"><i class="fa fa-search"></i></button></span>

                                </form>


                            </div>



                      {{--  <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home">HSN</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#menu1">SAC</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="home" class="container tab-pane active"><br>
                                <table class="table">
                                    <thead class="thead-light" style="background-color: #F1F1F1">
                                    <tr>
                                        <th scope="col">Section</th>
                                        <th scope="col">Chapter</th>
                                        <th scope="col">HSN Code</th>
                                        <th scope="col">Rate </th>
                                        <th scope="col">CESS %
                                        </th>
                                        <th scope="col">Related Export Inport HSN Codes </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                        <td>@mdo</td>
                                    </tr>

                                    </tbody>
                                </table>



                            </div>
                            <div id="menu1" class="container tab-pane fade"><br>
                                <table class="table">
                                    <thead class="thead-light" style="background-color: #F1F1F1">
                                    <tr>
                                        <th scope="col">Description</th>
                                        <th scope="col">SAC Code</th>
                                        <th scope="col">Rate %
                                        </th>
                                        <th scope="col">Also Check </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>

                                    </tr>

                                    </tbody>
                                </table>


                            </div>

                        </div>--}}





                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
