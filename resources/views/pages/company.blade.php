@extends('base')

@section('content')


    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Services</h1>
                    </div><!-- /.page-title-captions -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                            <li>Company Registration</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <!-- Services item -->
    <section class="flat-row section-iconbox padding2 bg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="title-section style3 left">
                        <h1 class="title">Company Registration</h1>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="title-section padding-left50">
                        <div class="sub-title style3">




                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-pie-chart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Company Registration</h5>
                            <p>Private Limited Company is the most prevalent and popular type of corporate
                                legal entity in India. Private limited company registration is governed by
                                the Ministry of Corporate Affairs, Companies Act, 2013 and the Companies
                                Incorporation Rules, 2014. To register a private limited company, a minimum
                                of two shareholders and two directors are required. A natural person can be both a
                                director and shareholder, while a corporate legal entity can only be a shareholder.
                                Further, foreign nationals, foreign corporate entities or NRIs are allowed to be
                                Directors and/or Shareholders of a Company with Foreign Direct Investment, making
                                it the preferred choice of entity for foreign promoters.

                            </p>

                            <p>Unique features of a private limited company like limited liability protection
                                to shareholders, ability to raise equity funds, separate legal entity status
                                and perpetual existence make it the most recommended type of business entity
                                for millions of small and medium sized businesses that are family owned or
                                professionally managed</p>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection