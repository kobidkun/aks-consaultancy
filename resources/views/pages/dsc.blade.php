



@extends('base')

@section('content')


    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Services</h1>
                    </div><!-- /.page-title-captions -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="/">Services</a></li>
                            <li>Digital Signature Certificate</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <!-- Services item -->
    <section class="flat-row section-iconbox padding2 bg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="title-section style3 left">
                        <h1 class="title">Digital Signature Certificate</h1>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="title-section padding-left50">
                        <div class="sub-title style3">
                            <p> Digital Signature Certificates or DSC or Digital Signature are being adopted by various government agencies and now is a statutory requirement in various applications.
                            </p>



                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="iconbox style3 box-shadow2">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-pie-chart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Digital Signature Certificate</h5>
                            <p>Capricorn offers different class of certificates to help organization and individuals secure online transactions with legal validity as per the Indian IT Act, 2000.
                            </p>
                            <p> Capricorn certificates conform to x.509 standard of Public Key Infrastructure (PKI) in India where in additionally these are issued as per IVG and IOG guidelines issued by the office of Controller of Certifying Authorities.
                            </p>
                            <br>
                            <h4>Type of Certificates</h4>

                            <ul class="list-round">
                                <li><strong>Sign</strong>
                                    <p class="ppad-left">The DSC could only be used for Signing a document. (The most popular Certificate)</p>
                                    <p class="ppad-left">The most popular usage is signing the PDF file for Tax Returns, MCA and other websites.</p>
                                </li>
                                <li><strong>Encrypt</strong>
                                    <p class="ppad-left">The DSC would be used to Encrypt a document, it is popularly used in tender portal, to help your company encrypt the documents and upload.</p>
                                    <p class="ppad-left">You could also use the certificate to encrypt and send classified information.</p>
                                    <p class="ppad-left">We are selling Encrypt certificate as a standalone product as well.</p>
                                    <div id="Validity-of-the-Certificate">&nbsp;</div>
                                </li>
                                <li><strong>Sign &amp; Encrypt</strong>
                                    <p class="ppad-left">You could buy both Sign &amp; Encrypt DSC by using this category.</p>
                                </li>
                            </ul>


                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection

