@extends('base')

@section('content')


<div class="page-title parallax parallax1">
    <div class="section-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title-heading">
                    <h1 class="title">Services</h1>
                </div><!-- /.page-title-captions -->
                <div class="breadcrumbs">
                    <ul>
                        <li class="home"><i class="fa fa-home"></i><a href="index.html">Services</a></li>
                        <li>LIC</li>
                    </ul>
                </div><!-- /.breadcrumbs -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.page-title -->

<!-- Services item -->
<section class="flat-row section-iconbox padding2 bg-section">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title-section style3 left">
                    <h1 class="title">LIC Services</h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="title-section padding-left50">
                    <div class="sub-title style3">
                        Life Insurance Corporation of India is one of the oldest and most trusted insurance companies.
                        LIC has earned customer satisfaction and reliability through several decades of its efficient services

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="iconbox style3 box-shadow2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="ti-pie-chart"></i>
                        </div>
                    </div>
                    <div class="box-content">
                        <h5  class="box-title">LIC Services</h5>
                        <p>

                            Life Insurance Corporation of India is one of the oldest and most trusted insurance companies. LIC has earned customer satisfaction and reliability through several decades of its efficient services.

                            Considering the diverse needs of the Indian population, it designs policies fitting all range and class of people. However, registering your policies and paying your premiums has become even easier through LIC e-services. Now, people do not have to go to their agents for premium payments or checking the status of your policies.

                            You can easily visit the LIC website in order to make payments of premium and checking your insurance status from time to time by yourself in a hassle-free manner.



                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


    @endsection
