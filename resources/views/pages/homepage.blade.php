@extends('base')

@section('content')


    <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container slide-overlay" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

        <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
        <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
            <div class="slotholder"></div>

            <ul><!-- SLIDE  -->

                <!-- SLIDE 1 -->
                <li data-index="rs-3050" data-transition="fade" data-slotamount="7" data-hideafterloop="0"
                    data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut"
                    data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns"
                    data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6=""
                    data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- <div class="overlay">
                    </div> -->
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('/slider/1.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption title-slide"
                         id="slide-3050-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-82','-82']"
                         data-fontsize="['60','60','50','33']"
                         data-lineheight="['70','70','50','35']"
                         data-fontweight="['700','700','700','700']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 16; white-space: nowrap;">Tally Adddon
                    </div>

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption sub-title position"
                         id="slide-3050-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['35','25','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-122','-122','-122','-122']"
                         data-fontsize="['16',16','16','14']"
                         data-fontweight="['600','600','600','600']"
                         data-width="['660','660','660','300']"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17; white-space: nowrap;text-transform:left;">Do more with Tally
                    </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption sub-title"
                         id="slide-3050-layer-4"
                         data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['12','12','-20,'-20']"
                         data-fontsize="['18',18','18','16']"
                         data-lineheight="['30','30','22','16']"
                         data-fontweight="['400','400','400','400']"
                         data-width="['800',800','800','450']"
                         data-height="none"
                         data-whitespace="['nowrap',normal','normal','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17; white-space: normal;">More then 10 addon to choose from
                    </div>

                    <a href="{{route('contact')}}" target="_self" class="tp-caption flat-button text-center"

                       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                       data-x="['center','center','center','center']" data-hoffset="['-502','-400','-240','-70']"
                       data-y="['middle','middle','middle','middle']" data-voffset="['106','106','80','50']"
                       data-fontweight="['700','700','700','700']"
                       data-width="['auto']"
                       data-height="['auto']"
                       style="z-index: 3;">Contact Us
                    </a><!-- END LAYER LINK -->

                    <a href="{{route('tally')}}" target="_self" class="tp-caption flat-button style2 text-center"

                       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                       data-x="['center','center','center','center']" data-hoffset="['-318','-200','-30','-70']"
                       data-y="['middle','middle','middle','middle']" data-voffset="['106','106','80','110']"
                       data-fontweight="['700','700','700','700']"
                       data-width="['auto']"
                       data-height="['auto']"
                       style="z-index: 3;">View More
                    </a><!-- END LAYER LINK -->
                </li>

                <!-- SLIDE 2 -->
                <li data-index="rs-3051" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- <div class="overlay">
                    </div> -->
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('/slider/2.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption title-slide"
                         id="slide-3051-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-82','-82','-82','-82']"
                         data-fontsize="['60','60','50','33']"
                         data-lineheight="['70','70','50','35']"
                         data-fontweight="['700','700','700','700']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[10,10,10,10]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 16; white-space: nowrap;color: #0b262f">GST Filling Service
                    </div>

                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption sub-title position"
                         id="slide-3051-layer-3"
                         data-x="['left','left','left','left']" data-hoffset="['35','25','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['-122','-122','-122','-122']"
                         data-fontsize="['16',16','16','14']"
                         data-fontweight="['600','600','600','600']"
                         data-width="['660','660','660','300']"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17; white-space: nowrap;text-transform:left;color: #000000">Simplifying GST compliance for your business

                    </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption sub-title"
                         id="slide-3051-layer-4"
                         data-x="['left','left','left','left']" data-hoffset="['35','20','50','50']"
                         data-y="['middle','middle','middle','middle']" data-voffset="['12','12','-20,'-20']"
                         data-fontsize="['18',18','18','16']"
                         data-lineheight="['30','30','22','16']"
                         data-fontweight="['400','400','400','400']"
                         data-width="['800',800','800','450']"
                         data-height="none"
                         data-whitespace="['nowrap',normal','normal','normal']"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 17; white-space: normal;color: #0b262f">We manage your books of accounts, generate E-way bills, maintain <br>different stock groups and file your GST returns
                    </div>

                    <a href="{{route('gst')}}" target="_self" class="tp-caption flat-button text-center"

                       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                       data-x="['center','center','center','center']" data-hoffset="['-502','-400','-240','-70']"
                       data-y="['middle','middle','middle','middle']" data-voffset="['106','106','80','50']"
                       data-fontweight="['700','700','700','700']"
                       data-width="['auto']"
                       data-height="['auto']"
                       style="z-index: 3;">OUR company
                    </a><!-- END LAYER LINK -->

                    <a href="{{route('contact')}}" target="_self" class="tp-caption flat-button style2 text-center"

                       data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'

                       data-x="['center','center','center','center']" data-hoffset="['-318','-200','-30','-70']"
                       data-y="['middle','middle','middle','middle']" data-voffset="['106','106','80','110']"
                       data-fontweight="['700','700','700','700']"
                       data-width="['auto']"
                       data-height="['auto']"
                       style="z-index: 3;">contact us
                    </a><!-- END LAYER LINK -->
                </li>

            </ul>
        </div>
    </div><!-- END REVOLUTION SLIDER -->

    <section class="flat-row section-iconbox">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="title-section style3 left">
                        <h1 class="title">Our Services</h1>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="title-section padding-left50">
                        <div class="sub-title style3">
                           </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="iconbox style3">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-pie-chart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Excel to Tally</h5>
                            <p>
                                It's convert Excel data as per Tally support format for importing it in Tally.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="iconbox style3">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-microphone"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Mutual Funds</h5>
                            <p>
                                Saving as much as possible and
                                Invest that income in the right places.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="iconbox style3">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-desktop"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">Life Insurance Corporation</h5>
                            <p>
                                Secure your future with Life Insurance Corporation

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="iconbox style3">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="ti-support"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5  class="box-title">GST Return</h5>
                            <p>
                                We offers an easy process to file GST Returns
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="flat-row v3 parallax parallax4">
        <div class="section-overlay style2"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section style2 color-white padding-lr180 text-center">
                        <h1 class="title">Request a call back.</h1>
                        <div class="sub-title">
                            Would you like to speak to one of our financial advisers?
                            Just submit your contact details and we’ll be in touch shortly. You can also email us at info@aksconsultancyservice.com if you prefer that type of communication.
                        </div>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="wrap-formrequest">
                <form id="contactform" class="contactform wrap-form clearfix" method="post" action="./contact/contact-process.php" novalidate="novalidate">
                    <span class="flat-input flat-select">
                        <select>
                            <option value="">Enquiry:*</option>

                        </select>
                    </span>
                    <span class="flat-input"><input name="name" type="text" value="" placeholder="Your Name:*" required="required"></span>
                    <span class="flat-input"><input name="phone" type="text" value="" placeholder="Phone Number:*" required="required"></span>
                    <span class="flat-input"><button name="submit" type="submit" class="flat-button" id="submit" title="Submit now">SUBMIT<i class="fa fa-angle-double-right"></i></button></span>
                </form>
            </div>
        </div>
    </section>


    @endsection


@section('footer')
    <!-- Revolution Slider -->
    <script src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="revolution/js/slider.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    @endsection