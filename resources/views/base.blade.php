
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Welcome to Aks consultancy Service </title>

    <meta name="author" content="themesflat.com">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/bootstrap.css')}}">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/style.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/responsive.css')}}">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/colors/color4.css')}}" id="colors">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/animate.css')}}">

    <!-- Animation headline Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheets/headline.css')}}">

    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{asset('revolution/css/layers.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('revolution/css/settings.css')}}">

    <!-- Favicon and touch icons  -->
    <link href="{{asset('icon/apple-touch-icon-48-precomposed.png')}}" rel="icon" sizes="48x48">
    <link href="{{asset('icon/apple-touch-icon-32-precomposed.png')}}" rel="icon">
    <link href="{{asset('icon/favicon.png')}}" rel="icon">

    <!--[if lt IE 9]>
    <script src="{{asset('javascript/html5shiv.js')}}"></script>
    <script src="{{asset('javascript/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body class="header_sticky">
<!-- Preloader -->
<div id="loading-overlay">
    <div class="loader"></div>
</div>

<!-- Boxed -->
<div class="boxed">



    <div class="flat-header-wrap">
        <!-- Header -->
        <div class="header widget-header clearfix">
            <div class="container">
                <div class="header-wrap clearfix">
                    <div class="row">
                        <div class="col-lg-3">
                            <div id="logo" class="logo">
                                <a href="/" rel="home">
                                    <img src="{{asset('images/logo.png')}}" alt="image">
                                </a>
                            </div><!-- /.logo -->

                        </div>
                        <div class="col-lg-9">
                            <div class="wrap-widget-header clearfix">
                                <aside  class="widget widget-info">
                                    <div class="btn-click">
                                        <a href="{{route('contact')}}" class="flat-button">GET A QUOTE</a>
                                    </div>
                                </aside>
                                <aside class="widget widget-info">
                                    <div class="textwidget clearfix">
                                        <div class="info-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="info-text">
                                            <h6>Venus More Hill Cart Road</h6>
                                            <p>Siliguri, IN</p>
                                        </div>
                                    </div>
                                </aside>
                                <aside class="widget widget-info">
                                    <div class="textwidget clearfix">
                                        <div class="info-icon">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <div class="info-text">
                                            <h6>+91 7864006464</h6>
                                            <p>info@aksconsultancyservice.com</p>
                                        </div>
                                    </div>
                                </aside>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.header-wrap -->

            </div>
        </div><!-- /.header -->

        <header id="header" class="header header-classic header-style1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-wrap-header">
                            <div class="nav-wrap clearfix">
                                <nav id="mainnav" class="mainnav">
                                    <ul class="menu">
                                        <li >
                                            <a href="/">Home</a>
                                        </li>

                                        <li><a href="#">Services</a>
                                            <ul class="submenu">
                                                <li><a href="{{route('tally')}}">Tally Addon</a></li>
                                                <li><a href="{{route('mf')}}">Mutual Funds</a></li>
                                                <li><a href="{{route('gst')}}">GST Filling</a></li>
                                                <li><a href="{{route('proj')}}">Project Finance</a></li>
                                                <li><a href="{{route('lic')}}">LIC</a></li>
                                                <li><a href="{{route('accounting')}}">Accounting</a></li>
                                                <li><a href="{{route('hsn')}}">Search HSN Code</a></li>
                                                <li><a href="{{route('dsc')}}">Digital Signature Certificate</a></li>
                                                <li><a href="{{route('company')}}">Company Registration</a></li>
                                                <li><a href="{{route('bus')}}">Vehicle Tracking</a></li>
                                            </ul><!-- /.submenu -->
                                        </li>

                                        <li><a href="{{route('public.video')}}">Video</a>
                                        <li><a href="{{route('blog.all')}}">Blog</a>
                                        <li><a href="{{route('contact')}}">Contact</a>
                                        </li>
                                    </ul><!-- /.menu -->
                                </nav><!-- /.mainnav -->


                                <div class="btn-menu">
                                    <span></span>
                                </div><!-- //mobile menu button -->
                            </div><!-- /.nav-wrap -->

                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>



@yield('content')



    <!-- Footer -->
    <footer class="footer widget-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget-logo">
                        <div id="logo-footer" class="logo">
                            <a href="/" rel="home">
                                <img src="{{asset('/images/logo-light.png')}}" alt="image">
                            </a>
                        </div>
                        <p>We provide Tax GST Accounting Financial Services</p>
                        <ul class="flat-information">
                            <li><i class="fa fa-map-marker"></i><a href="#">Venus More Hill Cart Road, Siliguri</a></li>
                            <li><i class="fa fa-phone"></i><a href="#">+91 7864006464</a></li>
                            <li><i class="fa fa-envelope"></i><a href="#">info@aksconsultancyservice.com</a></li>
                        </ul>
                    </div>
                </div><!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-out-link clearfix">
                        <h5 class="widget-title">Our Links</h5>
                        <ul class="one-half">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="{{route('tally')}}">Tally Addon</a></li>
                            <li><a href="{{route('mf')}}">Mutual Funds</a></li>
                            <li><a href="{{route('gst')}}">GST Filling</a></li>

                        </ul>

                    </div>
                </div><!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-out-link clearfix">
                        <h5 class="widget-title">Our Links</h5>

                        <ul class="one-half">
                            <li><a href="{{route('proj')}}">Project Finance</a></li>
                            <li><a href="{{route('lic')}}">LIC</a></li>
                            <li><a href="{{route('accounting')}}">Accounting</a></li>
                            <li><a href="{{route('contact')}}">Contact</a></li>
                        </ul>
                    </div>
                </div><!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-letter">
                        <h5 class="widget-title">Newsletter</h5>
                        <p class="info-text">Subscribe our newsletter gor get noti-fication about new updates, etc.</p>
                        <form id="subscribe-form" class="flat-mailchimp" method="post" action="#" data-mailchimp="true">
                            <div class="field clearfix" id="subscribe-content">
                                <p class="wrap-input-email">
                                    <input type="text" tabindex="2" id="subscribe-email" name="subscribe-email" placeholder="Enter Your Email">
                                </p>
                                <p class="wrap-btn">
                                    <button type="button" id="subscribe-button" class="flat-button subscribe-submit" title="Subscribe now">SUBSCRIBE</button>
                                </p>
                            </div>
                            <div id="subscribe-msg"></div>
                        </form>
                    </div>
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->logofooter@2x.png
    </footer>

    <!-- Bottom -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copyright">
                        <p>@2018 AKS Consultancy Service Designed by <a href="https://www.tecions.com/" target="_blank">Tecions</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="social-links style2 text-right">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- bottom -->

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>
</div>

<!-- Javascript -->
<script src="{{asset('javascript/jquery.min.js')}}"></script>

<script src="{{asset('javascript/tether.min.js')}}"></script>
<script src="{{asset('javascript/bootstrap.min.js')}}"></script>
<script src="{{asset('javascript/jquery.easing.js')}}"></script>
<script src="{{asset('javascript/jquery-waypoints.js')}}"></script>
<script src="{{asset('javascript/jquery-validate.js')}}"></script>
<script src="{{asset('javascript/jquery.cookie.js')}}"></script>

<script src="{{asset('javascript/owl.carousel.js')}}"></script>
<script src="{{asset('javascript/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('javascript/headline.js')}}"></script>
<script src="{{asset('javascript/parallax.js')}}"></script>

<script src="{{asset('javascript/main.js')}}"></script>

@yield('footer');
</body>
</html>