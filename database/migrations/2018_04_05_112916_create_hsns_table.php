<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHsnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hsns', function (Blueprint $table) {
            $table->increments('id');
            $table->text('chapter')->nullable();
            $table->text('code')->nullable();
            $table->text('xrate')->nullable();
            $table->text('rate')->nullable();
            $table->text('cess')->nullable();
            $table->text('effective_from')->nullable();
            $table->text('description')->nullable();
            $table->text('related')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hsns');
    }
}
